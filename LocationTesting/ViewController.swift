//
//  ViewController.swift
//  LocationTesting
//
//  Created by Noa on 27/04/2021.
//

import UIKit
import MapKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var goBtn: UIButton!
    @IBOutlet weak var numberTF: UITextField!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var deleteLogsBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let maxFileNum = UserDefaults.standard.integer(forKey: UserDefKeys.LogfileNumber.rawValue)
        numberTF.placeholder = "max: \(maxFileNum)"
        KeepersWebFilteringLoggerWriter.log("file number: \(maxFileNum)")
        textView.text = KeepersWebFilteringLoggerWriter.readLog()
    }
    
    @IBAction func deleteLogsClicked(_ sender: Any) {
        textView.text = ""
    }
    
    @IBAction func refreshLogsClicked(_ sender: Any) {
        textView.text = KeepersWebFilteringLoggerWriter.readLog()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        go()
        return true;
    }
    
    
    @IBAction func goClicked(_ sender: Any) {
        let _ = textFieldShouldReturn(numberTF)
    }
    
    func go() {
        if let number = Int(numberTF.text ?? "no number") {
            textView.text = KeepersWebFilteringLoggerWriter.readLog(from: number)
        }
    }
    
}

