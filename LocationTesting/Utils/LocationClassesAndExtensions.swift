//
//  LocationClassesAndExtensions.swift
//  LocationTesting
//
//  Created by Noa on 29/04/2021.
//

import Foundation
import MapKit

enum StrengthType : String , Codable {
    case easy       = "easy"
    case medium     = "medium"
    case hard       = "heavy"
    case events     = "events"
    case webFilter  = "webFilter"
    case none       = "none"
    
    init(from severity: String?){
        guard let severity = severity else { self = .none ; return }
        switch severity {
        case StrengthType.easy.rawValue:        self = .easy
        case StrengthType.medium.rawValue:      self = .medium
        case StrengthType.hard.rawValue:        self = .hard
        case StrengthType.events.rawValue:      self = .events
        case StrengthType.webFilter.rawValue:   self = .webFilter
        default:                                self = .none
        }
    }
    
    func toString() -> String { return self.rawValue }
}

enum AccuracyLevel : String , Codable {
    case low    = "low"
    case medium = "medium"
    case high   = "high"
    init(string: String){
        switch string {
        case AccuracyLevel.low.rawValue    : self = .low
        case AccuracyLevel.medium.rawValue : self = .medium
        case AccuracyLevel.high.rawValue   : self = .high
        default: self = .high
        }
    }
    func localizedFullSentence() -> String {
        var before = self.rawValue + " "
        return before + "level location accuracy."
    }
}


extension NSCoder {
    func decodeStrength(forKey key: String) -> StrengthType {
        if let StringRepresentation = self.decodeObject(forKey: key) as? String {
            return StrengthType(from: StringRepresentation)
        }
        return .none
    }
    func decodeAccuracy(forKey key: String) -> AccuracyLevel {
        let accuracyStr = self.decodeObject(forKey: key) as? String ?? AccuracyLevel.high.rawValue
        return AccuracyLevel(string: accuracyStr)
    }
}


extension MKPolyline {
    convenience init(from locationList : [CLLocation
    ] ) {
        let points = locationList.compactMap { (location) -> CLLocationCoordinate2D? in
            return location.coordinate
        }
        self.init(coordinates: points, count: points.count)
    }
}

public class LocationHelperUtil {
    
    static func getAdressName(coords: CLLocation, onComplete:@escaping(String)->()){
        
        CLGeocoder().reverseGeocodeLocation(coords) { (placemark, error) in
            if error != nil {
                onComplete("")
            } else {
                
                let place = placemark! as [CLPlacemark]
                if place.count > 0 {
                    let place = placemark![0]
                    var adressString : String = ""
                    if place.thoroughfare != nil {
                        adressString = adressString + place.thoroughfare! + ", "
                    }
                    if place.subThoroughfare != nil {
                        adressString = adressString + place.subThoroughfare! + "\n"
                    }
                    if place.locality != nil {
                        adressString = adressString + place.locality! + " - "
                    }
                    if place.postalCode != nil {
                        adressString = adressString + place.postalCode! + "\n"
                    }
                    if place.subAdministrativeArea != nil {
                        adressString = adressString + place.subAdministrativeArea! + " - "
                    }
                    if place.country != nil {
                        adressString = adressString + place.country!
                    }
                    
                    onComplete(adressString)
                }
            }
        }
    }
    
}


class RestrictedAreaOverlay: MKCircle, NSCoding {
    static let KEY_RESTRICTED_AREA_ARRAY = "RestrictedAnnotationsAsData"
    static let KEY_IS_RESTRICTED = "isRestricted"
    static let KEY_LONGITUDE = "cordLongitude"
    static let KEY_LATITUDE = "cordLatitude"
    static let KEY_RADIUS = "areaRadius"
    static let KEY_ID = "areaId"
    
    var id : Int64 = -1
    var isRestricted : Bool = true
    
    convenience init(center coord: CLLocationCoordinate2D, radius: CLLocationDistance,_ isRestricted : Bool) {
        self.init(center: coord, radius: radius)
        self.isRestricted = isRestricted
    }
    convenience init(id: Int64, center coord: CLLocationCoordinate2D, radius: CLLocationDistance,_ isRestricted : Bool) {
        self.init(center: coord, radius: radius)
        self.isRestricted = isRestricted
        self.id = id
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id,forKey:RestrictedAreaOverlay.KEY_ID)
        aCoder.encode(self.isRestricted,forKey:RestrictedAreaOverlay.KEY_IS_RESTRICTED)
        aCoder.encode(self.coordinate.longitude,forKey: RestrictedAreaOverlay.KEY_LONGITUDE)
        aCoder.encode(self.coordinate.latitude,forKey: RestrictedAreaOverlay.KEY_LATITUDE)
        aCoder.encode(self.radius,forKey:RestrictedAreaOverlay.KEY_RADIUS)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeInt64(forKey: RestrictedAreaOverlay.KEY_ID)
        let isRestricted = aDecoder.decodeBool(forKey: RestrictedAreaOverlay.KEY_IS_RESTRICTED)
        let longitude = aDecoder.decodeDouble(forKey: RestrictedAreaOverlay.KEY_LONGITUDE)
        let latitude = aDecoder.decodeDouble(forKey: RestrictedAreaOverlay.KEY_LATITUDE)
        let cords = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let radius = aDecoder.decodeDouble(forKey: RestrictedAreaOverlay.KEY_RADIUS)
        if id != 0 {
            self.init(id: id, center: cords, radius: radius, isRestricted)
        }else{
            self.init(center: cords, radius: radius, isRestricted)
        }
    }
}

class RestrictedAreaAnnotation : MKPointAnnotation {
    var isRestricted = true
    convenience init (_ coordinate : CLLocationCoordinate2D,_ isRestricted : Bool) {
        self.init()
        self.coordinate = coordinate
        self.isRestricted = isRestricted
    }
}
extension CLLocationCoordinate2D: Equatable {}

public func ==(lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
    return (lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude)
}
extension MKMapView {
    func addAnnotation(with new : MKAnnotation? ) {
        if let annotation = new {
            self.addAnnotation(annotation)
        }
    }
    func setRegion (at location : CLLocation) {
        let center = location.coordinate
        let region = MKCoordinateRegion(center: center, latitudinalMeters: 500, longitudinalMeters: 500)
        self.setRegion(region, animated: true)
    }
    func getOverlay( withCoordinate coordinate : CLLocationCoordinate2D ) -> RestrictedAreaOverlay? {
        return self.overlays.first(where: { elem -> Bool in
            return elem.coordinate == coordinate
        }) as? RestrictedAreaOverlay
    }
    func getView (at coordinate : CLLocationCoordinate2D? ) -> MKAnnotationView?  {
        if let coordinate = coordinate {
            let allAnnotations = self.annotations
            if let annotation = allAnnotations.first(where: { $0.coordinate == coordinate}) {
                return self.view(for: annotation)
            }
        }
        return nil
    }
    
    func removeAll()
    {
        self.removeAnnotations(self.annotations)
        self.removeOverlays(self.overlays)
    }
    
    func removeOptionalAnnotation(_ annotation: MKAnnotation?){
        if let annotation = annotation {
            self.removeAnnotation(annotation)
        }
    }
    func addAreaAndAnnotation(from area : RestrictedAreaOverlay) -> RestrictedAreaOverlay {
        let cords           = area.coordinate
        let isRestricted    = area.isRestricted
        let radius          = area.radius
        return addAreaAndAnnotation(center: cords, radius: radius, isRestricted: isRestricted)
    }
    func addAreaAndAnnotation(center : CLLocationCoordinate2D , radius: CLLocationDistance , isRestricted : Bool )  -> RestrictedAreaOverlay {
        let areaCircle = RestrictedAreaOverlay(center: center, radius: CLLocationDistance(radius), isRestricted)
        let annotation = RestrictedAreaAnnotation(center,isRestricted )
        self.addOverlay(areaCircle)
        self.addAnnotation(annotation)
        return areaCircle
    }
    func loadRestrictedAreas (forKey key: String) -> [RestrictedAreaOverlay]? {
        if let loadedAreas = UserDefaults.standard.data(forKey: key ){
            do {
                if let restricredAreas = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(loadedAreas) as? [RestrictedAreaOverlay] {
                    for area in restricredAreas {
                        let _ = self.addAreaAndAnnotation(from : area )
                    }
                    return restricredAreas
                }
            } catch { }
        }
        return nil
    }
    func location(from gesture : UIGestureRecognizer) -> CLLocationCoordinate2D {
        let touchPoint = gesture.location(in: self)
        return self.convert(touchPoint, toCoordinateFrom: self)
    }
    func viewFor(annotation: CustomPointAnnotation) -> AnnotationView? {
        var annotationView: AnnotationView?
        let reuseIdentifier = annotation.isLastPin ? "lastLocation" : "pin"
        annotationView = dequeueReusableAnnotationView(withIdentifier: reuseIdentifier) as? AnnotationView
        if annotationView == nil {
            annotationView = AnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
        }
        annotationView?.image = UIImage(named: annotation.pinCustomImageName ?? "")
        annotationView?.setViewToDefaultZOrder()
        annotationView?.setImageSize(last: annotation.isLastPin)
        if annotation.isLastPin {
            annotationView?.bringViewToFront()
        }
        
//        annotationView?.setSelected(false, animated: false)
        
        
        if !annotation.isLastPin {
            ///Adding the number label in the middel of this view
            let label = UILabel()
            label.frame = CGRect(x: 0, y: 0, width: 140, height: 38)
            label.text = ""
            label.textColor = UIColor(displayP3Red: 41/255.0, green: 41/255.0, blue: 41/255.0, alpha: 1.0)
            label.sizeToFit()
            
            annotationView?.addSubview(label)
            label.center = annotationView!.center
            label.center.x = 0.5 * annotationView!.frame.size.width;
            label.center.y = 0.5 * annotationView!.frame.height;
            label.layer.zPosition = 2
            
        }
        
        return annotationView
    }
    func viewFor(annotation: RestrictedAreaAnnotation) -> MKAnnotationView? {
        let isRestricted = annotation.isRestricted
        let reuseId   = isRestricted ? "redIdentifier" : "greenIdentifier"
        let imageName = isRestricted ? "location_red" : "location_green-1"
        let anView = dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView != nil {
            anView?.annotation = annotation
            return anView
        }
        let restrictedAnnotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
        restrictedAnnotationView.image = UIImage(named: imageName)
        restrictedAnnotationView.centerOffset = CGPoint(x: 0, y: -restrictedAnnotationView.frame.size.height / 2)
        return restrictedAnnotationView
    }
    func createRestrictedAreaRenderer(on overlay: MKOverlay) -> MKCircleRenderer? {
        if let circleArea = overlay as? RestrictedAreaOverlay {
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.fillColor = circleArea.isRestricted ? UIColor.red :  UIColor.green
            circleRenderer.alpha = 0.3
            return circleRenderer
        }
        return nil
    }
    func createPolylineRenderer(on overlay: MKOverlay) -> MKPolylineRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        polylineRenderer.lineDashPattern = [1, 5]
        polylineRenderer.strokeColor = UIColor.lightGray
        polylineRenderer.lineWidth = 3
        return polylineRenderer
    }
}
