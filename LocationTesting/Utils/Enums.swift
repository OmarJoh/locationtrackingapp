//
//  Enums.swift
//  LocationTesting
//
//  Created by Noa on 02/05/2021.
//

import Foundation

enum UserDefKeys: String {
    case LocationsArray
    case allLocationsDataSource
    case allLocationsDictionary
    case AllLocationsArray
    case LogfileNumber
    case LogfileWithNumber //used to print logs of given number of file
}
