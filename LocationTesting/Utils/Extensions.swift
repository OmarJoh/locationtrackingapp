//
//  Extensions.swift
//  LocationTesting
//
//  Created by Noa on 28/04/2021.
//

import Foundation
import UIKit

extension UIButton {
    
    func addAction(for controlEvents: UIControl.Event, _ closure: @escaping ()->()) {
        let sleeve = ClosureSleeve(closure)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
        objc_setAssociatedObject(self, String(format: "[%d]", arc4random()), sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
}
extension Double {
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
    var degreesToRadians : CGFloat {
        return CGFloat(self) * CGFloat(Double.pi) / 180.0
    }
}

extension Date {
    
    
    var millisecondsSince1970: Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds: Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    func diffrenceInMinutesFromNow() -> Int{
        let calendar = Calendar.current
        let timeComponents = calendar.dateComponents([.hour, .minute], from: self)
        let nowComponents = calendar.dateComponents([.hour, .minute], from: Date())
        return calendar.dateComponents([.minute], from: timeComponents, to: nowComponents).minute!
    }
    func diffrenceInHoursFromNow() -> Int{
        let calendar = Calendar.current
        let timeComponents = calendar.dateComponents([.day,.hour], from: self)
        let nowComponents = calendar.dateComponents([.day,.hour], from: Date())
        return calendar.dateComponents([.hour], from: timeComponents, to: nowComponents).hour!
    }
    func isToday() -> Bool{
        let calendar = Calendar.current
        let timeComponents = calendar.dateComponents([.day,.month, .year], from: self)
        let nowComponents = calendar.dateComponents([.day,.month, .year], from: Date())
        return timeComponents.day == nowComponents.day && timeComponents.month == nowComponents.month && timeComponents.year == nowComponents.year
    }
}

class ClosureSleeve {
    let closure: ()->()
    
    init (_ closure: @escaping ()->()) {
        self.closure = closure
    }
    
    @objc func invoke () {
        closure()
    }
}
