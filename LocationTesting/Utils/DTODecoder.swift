//
//  DTODecoder.swift
//  keepers
//
//  Created by Keepers on 6/4/19.
//  Copyright © 2019 Keepers. All rights reserved.
//

import Foundation
public class DTODecoder {
    
    fileprivate static let decoder = JSONDecoder()
    
    public static func encodeDTO<T>(_ obj : T) -> Data? where T:Codable {
        return try? JSONEncoder().encode(obj)
    }
    
    public static func decodeDTO<T>(data: Data?) -> T? where T:Codable {
        guard let data = data else { return nil }
        do {
            //if want to debug then uncomment the next 2 lines (and comment "return try..." line)
            //            let d : Codable = try decoder.decode(T.self, from: data)
            //            return d as! T
            return try decoder.decode(T.self, from: data)
        }
        catch {
        }
        return nil
    }
    
    public static func decodeDTO<T>(dictionary: [AnyHashable:Any]?) -> T? where T:Codable {
        guard let dictionary = dictionary else { return nil }
        do { let data = try JSONSerialization.data(withJSONObject: dictionary)
            return try JSONDecoder().decode(T.self, from: data)
        }catch {   return nil}
    }
    
    public static func convertToDictionary(text: String) -> [[String: Any]]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]
            } catch {
                
            }
        }
        return nil
    }
    
    public static func convertToDictionary(data: Data) -> [[String: Any]]? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]
        } catch {
            
        }
        return nil
    }
}
