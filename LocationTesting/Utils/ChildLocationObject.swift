//
//  ChildLocationObject.swift
//  keepers
//
//  Created by Keepers on 4/12/18.
//  Copyright © 2018 Keepers. All rights reserved.
//

import Foundation
import MapKit
class CustomPointAnnotation: MKPointAnnotation {
    var pinCustomImageName: String!
    var isLastPin : Bool {
        get {
            return pinCustomImageName == "New_GPS_ios" || pinCustomImageName == "New_GPS_ios"
        }
    }
    convenience init(imageName : String ,at coordinate : CLLocationCoordinate2D) {
        self.init()
        self.pinCustomImageName = imageName
        self.coordinate = coordinate
    }
    convenience init(imageName : String ,at coordinate : CLLocationCoordinate2D , title: String) {
        self.init(imageName: imageName, at: coordinate)
        self.title = title
    }
}

class AnnotationView: MKAnnotationView {
    static let trailImageSize : CGFloat = 18
    override class var layerClass: AnyClass {
        return ZPositionableLayer.self
    }
    
    var stickyZPosition: CGFloat {
        get { return (self.layer as! ZPositionableLayer).stickyZPosition }
        set { (self.layer as! ZPositionableLayer).stickyZPosition = newValue }
    }
    func bringViewToFront() {
        superview?.bringSubviewToFront(self)
        stickyZPosition = CGFloat(1)
    }
    func setViewToDefaultZOrder() {
        stickyZPosition = CGFloat(0)
    }
    func setImageSize(last isLast : Bool) {
        if isLast { return }
        frame.size = CGSize(width: AnnotationView.trailImageSize,
                            height: AnnotationView.trailImageSize)
    }
}

class ZPositionableLayer: CALayer {
    override var zPosition: CGFloat {
        get { return super.zPosition }
        set {/* this is the magic - just do nothing */}
    }
    
    var stickyZPosition: CGFloat {
        get { return super.zPosition }
        set { super.zPosition = newValue }
    }
}


public class ChildLocationObject: NSObject, NSCoding {
    static let LOCATION_ADDRESS_ERROR = NSLocalizedString("Could not find address",
                                                          comment: "Error converting location to address text - TimeLine table")
    var lat: Double = -1
    var lon: Double = -1
    var time: Int64 = -1
    var isOutlier: Bool = false
    var givenAddress : String =  NSLocalizedString("Could not find address", comment: "Error converting location to address text - TimeLine table")
    var accuracyLevel : AccuracyLevel = .high
    
    convenience init(from locationDict : NSDictionary?){
        let location = locationDict ?? NSDictionary()
        let givenAddress = location["address"]      as? String  ?? ChildLocationObject.LOCATION_ADDRESS_ERROR
        let latitude     = location["latitude"]     as? Double  ?? 0.0
        let longitude    = location["longitude"]    as? Double  ?? 0.0
        let time   = Int64(location["dateCreated"]  as? Double  ?? 0.0)
        let isOutlier    = location["isOutlier"]    as? Bool    ?? false
        let accuracy     = location["accuracy"]     as? String  ?? AccuracyLevel.high.rawValue
        let accuracyLevel = AccuracyLevel(string: accuracy)
        self.init(lat: latitude, lon:longitude,time: time, givenAddress: givenAddress, isOutlier: isOutlier,accuracy: accuracyLevel)
    }
    init(lat: Double, lon: Double, time: Int64 , givenAddress: String, isOutlier: Bool, accuracy: AccuracyLevel) {
        self.lat = lat
        self.lon = lon
        self.time = time
        self.givenAddress = givenAddress
        self.isOutlier = isOutlier
        self.accuracyLevel = accuracy
    }
    
    required public init(coder decoder: NSCoder) {
        self.lat = decoder.decodeDouble(forKey: "lat")
        self.lon = decoder.decodeDouble(forKey: "lon")
        self.time = decoder.decodeInt64(forKey: "dateCreated")
        self.givenAddress = decoder.decodeObject(forKey: "givenAddress") as? String ?? ""
        self.isOutlier = decoder.decodeBool(forKey: "isOutlier")
        self.accuracyLevel = decoder.decodeAccuracy(forKey: "accuracy")
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.lat, forKey: "lat")
        aCoder.encode(self.lon, forKey: "lon")
        aCoder.encode(self.time, forKey: "dateCreated")
        aCoder.encode(self.givenAddress, forKey: "givenAddress")
        aCoder.encode(self.isOutlier, forKey: "isOutlier")
        aCoder.encode(self.accuracyLevel.rawValue, forKey: "accuracy")
    }
    
    func asCLLocationCoordinate2D () -> CLLocationCoordinate2D? {
        guard lat != -1 && lon != -1 else { return nil }
        return CLLocationCoordinate2D(latitude: lat, longitude: lon)
    }
    func asCLLocation () -> CLLocation? {
        guard lat != -1 && lon != -1 else { return nil }
        let retValue = CLLocation(coordinate: CLLocationCoordinate2D(latitude: self.lat, longitude: self.lon), altitude: 0, horizontalAccuracy: 0, verticalAccuracy: 0, timestamp: Date(milliseconds: self.time))
        return retValue
    }
    func distanceInMeters(to p2: ChildLocationObject) -> Double {
        if( self.lat == -1 || self.lon == -1
            || p2.lat == -1 || p2.lon == -1) {
            return -1
        }
        let coordinate1 = CLLocation(latitude: lat, longitude: lon)
        let coordinate2 = CLLocation(latitude: p2.lat, longitude: p2.lon)
        
        let distanceInMeters = coordinate1.distance(from: coordinate2)
        return abs(distanceInMeters)
    }
    func isEqual (p2: ChildLocationObject) -> Bool {
        return p2.lat == lat &&
                p2.lon == lon &&
                 p2.time == time
    }
    static func getAddress (from placeMark : CLPlacemark? ) -> String {
        if let placeMark = placeMark {
            var returnValue = ""
            if let name = placeMark.addressDictionary?["Name"] as? String {
                returnValue = name
            }
            if let city = placeMark.addressDictionary?["City"] as? String {
                returnValue = returnValue.isEmpty ? city : returnValue + ", " + city
            }
            if let country = placeMark.addressDictionary?["Country"] as? String {
                returnValue = returnValue.isEmpty ? country : returnValue + ", " + country
            }
            return returnValue
        }
        return LOCATION_ADDRESS_ERROR
    }
}


