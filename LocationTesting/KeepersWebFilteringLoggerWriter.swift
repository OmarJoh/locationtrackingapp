//
//  KeepersWebFilteringLoggerWriter.swift
//  KeepersWebFilteringSDK
//
//  Created by Omarj on 02/09/2020.
//  Copyright © 2020 Keepers. All rights reserved.
//

import Foundation
let APP_GROUP = "group.com.keepers.KeepersVPN-MVP"


class KeepersWebFilteringLoggerWriter {
    
    static var logFile: URL? {
//        let appGroupData = KeyChain.load(key: .APP_GROUP)
//        var appGroupStr = String(data: appGroupData ?? Data(), encoding: .utf8) ?? ""
//        appGroupStr = KeepersLogger.getAppGroup()
//        guard let documentsDirectory = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: appGroupStr) else { return nil }
        guard let documentsDirectory = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: APP_GROUP) else { return nil }
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let dateString = formatter.string(from: Date())
        let number: String = String(UserDefaults.standard.integer(forKey: UserDefKeys.LogfileNumber.rawValue))
        let fileName = "\(dateString + number).log"
        return documentsDirectory.appendingPathComponent(fileName)
    }
    
    
    //called when want to print specific logfile of given number
    static var logFileWithNumber: URL? {
        guard let documentsDirectory = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: APP_GROUP) else { return nil }
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let dateString = formatter.string(from: Date())
        let number: String = String(UserDefaults.standard.integer(forKey: UserDefKeys.LogfileWithNumber.rawValue))
        let fileName = "\(dateString + "-" + number).log"
        return documentsDirectory.appendingPathComponent(fileName)
    }
    
    static func readLog() -> String {
        guard let logFile = logFile else {
            return ""
        }
        
        do {
            let text2 = try String(contentsOf: logFile, encoding: .utf8)
            return text2
        }
        catch {/* error handling here */}
        
        return ""
    }
    
    static func readLog(from fileNumber: Int) -> String {
        UserDefaults.standard.set(fileNumber, forKey: UserDefKeys.LogfileWithNumber.rawValue)
        guard let logFile = logFileWithNumber else {
            return ""
        }
        
        
        do {
            let text2 = try String(contentsOf: logFile, encoding: .utf8)
            return text2
        }
        catch {/* error handling here */}
        
        return ""
    }
    
    static func log(_ message: String) {
        guard let logFile = logFile else {
            return
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        let timestamp = formatter.string(from: Date())
        guard let data = (timestamp + ": " + message + "\n").data(using: String.Encoding.utf8) else { return }
        
        if FileManager.default.fileExists(atPath: logFile.path) {
            if let fileHandle = try? FileHandle(forWritingTo: logFile) {
                fileHandle.seekToEndOfFile()
                fileHandle.write(data)
                fileHandle.closeFile()
            }
        } else {
            try? data.write(to: logFile, options: .atomicWrite)
        }
    }
    
    static func deleteContents() {
        guard let logFile = logFile else {
            return
        }
        let text = ""
        guard let data = ("").data(using: String.Encoding.utf8) else { return }
        if FileManager.default.fileExists(atPath: logFile.path) {
            if let fileHandle = try? FileHandle(forWritingTo: logFile) {
                //                fileHandle.seekToEndOfFile()
                fileHandle.write(data)
                fileHandle.closeFile()
            }
        } else {
            try? data.write(to: logFile, options: .atomicWrite)
        }
        try? data.write(to: logFile)
        //        text.write(toFile: logFile, atomically: false, encoding: .utf8)
    }
    
    static func newLogfile() {
        let number: Int = UserDefaults.standard.integer(forKey: UserDefKeys.LogfileNumber.rawValue)
        UserDefaults.standard.set(number + 1, forKey: UserDefKeys.LogfileNumber.rawValue)
        }
}
