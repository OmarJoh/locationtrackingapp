//
//  TimeLineTableViewCell.swift
//  keepers
//
//  Created by Noy's Mac on 26.11.2017.
//  Copyright © 2017 Keepers. All rights reserved.
//

import UIKit

class TimeLineTableViewCell: UITableViewCell {

    @IBOutlet weak var childAddress: UILabel!
    @IBOutlet weak var lastSeen: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var chainImg: UIImageView!
    @IBOutlet weak var locationNumberLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        timeLbl.text = "Now"
        childAddress.text = "Empty_address_key"
        lastSeen.text = "Last_Seen_location_key"
        
        timeLbl.font = UIFont.systemFont(ofSize: 14)
        lastSeen.font = UIFont.systemFont(ofSize: 14)
        childAddress.font = UIFont.systemFont(ofSize: 18)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
