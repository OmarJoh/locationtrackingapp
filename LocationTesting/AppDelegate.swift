//
//  AppDelegate.swift
//  LocationTesting
//
//  Created by Noa on 27/04/2021.
//

import UIKit
import MapKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {
    class var shared: AppDelegate { return UIApplication.shared.delegate as! AppDelegate }

    private let smallRegionID = "DefaultID"
    private let largeRegionID = "LargeRegionID"
    
    private let current_region = "region_current"
    
    private let region1ID = "region_1ID"
    private let region2ID = "region_2ID"
    private let region3ID = "region_3ID"
    private let region4ID = "region_4ID"
    
    private let region1ID_BIG = "region_1ID_BIG"
    private let region2ID_BIG = "region_2ID_BIG"
    private let region3ID_BIG = "region_3ID_BIG"
    private let region4ID_BIG = "region_4ID_BIG"
    //outer regions identifiers
    
    private let region1ID_BIG_OUTER = "region_1ID_BIG_OUTER"
    private let region2ID_BIG_OUTER = "region_2ID_BIG_OUTER"
    private let region3ID_BIG_OUTER = "region_3ID_BIG_OUTER"
    private let region4ID_BIG_OUTER = "region_4ID_BIG_OUTER"
    private let region5ID_BIG_OUTER = "region_5ID_BIG_OUTER"
    private let region6ID_BIG_OUTER = "region_6ID_BIG_OUTER"
    private let region7ID_BIG_OUTER = "region_7ID_BIG_OUTER"
    private let region8ID_BIG_OUTER = "region_8ID_BIG_OUTER"
    
    private let region_allRegion = "region_allRegion"

    
    private let smallRegionRadius = 150.0
    private let largeRegionRadius = 320.0

    let locationManagerInstance = CLLocationManager()

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        KeepersWebFilteringLoggerWriter.log("didFinishLaunchingWithOptions")

        startTrackingLocation()
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        KeepersWebFilteringLoggerWriter.log("applicationWillTerminate")
//        self.locationManagerInstance.startMonitoringSignificantLocationChanges()
    }
    
    func startTrackingLocation(){
        locationManagerInstance.delegate = self
        locationManagerInstance.desiredAccuracy = kCLLocationAccuracyBest
        locationManagerInstance.distanceFilter = 10
        locationManagerInstance.allowsBackgroundLocationUpdates = true
        locationManagerInstance.pausesLocationUpdatesAutomatically = false
        locationManagerInstance.stopMonitoringSignificantLocationChanges()
//        locationManagerInstance.requestAlwaysAuthorization()
        self.locationManagerInstance.startUpdatingLocation()
        
//        if locationTimer == nil {
//            locationTimer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true, block: {_ in
//                if UserDefaultsServiceChild().childIsLoggedIn {
//                    KeepersWebFilteringLoggerWriter.log("##\nin timer, calling startUpdatingLocation\n##")
//                    self.locationManagerInstance.startUpdatingLocation()
//                }else{
//                    self.locationManagerInstance.stopUpdatingLocation()
//                    locationTimer?.invalidate()
//                    locationTimer = nil
//                }
//            })
//        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways: break
        case .notDetermined:
            self.locationManagerInstance.requestAlwaysAuthorization()
        case .authorizedWhenInUse, .denied:
            self.locationManagerInstance.requestAlwaysAuthorization()
        case .restricted:
            self.locationManagerInstance.requestAlwaysAuthorization()
        @unknown default:
            self.locationManagerInstance.requestAlwaysAuthorization()
        }
//        notifyParentWhenPermissionChanged(status)
        //self.locationPermissionsService.didChangeAuthorizationStatus(status)
    }
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        print("stopped location updates")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("received error when was receiving location changes")
    }
    
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
        print("resumed location updates")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
//        guard let lastLocation = locations.last else { return }
        
//        if lastLocation.horizontalAccuracy > 65.0 {
//            manager.stopUpdatingLocation()
//            manager.startUpdatingLocation()
//            return
//        }

        KeepersWebFilteringLoggerWriter.log("didUpdateLocations, manager.location = \(String(describing: manager.location))")
        KeepersWebFilteringLoggerWriter.log("manager.location?.horizontalAccuracy = \(String(describing: manager.location?.horizontalAccuracy))")
        //
        
         
        if let location = manager.location {
//            let now = Date()
            
            saveLocation(location)
            NotificationCenter.default.post(
                name: NSNotification.Name(rawValue: "Got A new Location to update map"),
                object: self)
            createNewRegionsForLocationTracking(location: manager.location!)
            
//            self.locationManagerInstance.stopUpdatingLocation()
            
            ////////
//            if (manager.location!.timestamp.isBeforeDate(now, granularity: .day) ||
//                manager.location!.timestamp.isAfterDate(now, granularity: .day)) ||
//                //same day, same hour, minutes is before now
//                (manager.location!.timestamp.isInside(date: now, granularity: .day) &&
//                    manager.location!.timestamp.isInside(date: now, granularity: .hour) && manager.location!.timestamp.isBeforeDate(now, granularity: .minute)) ||
//                //same day, hour is before now
//                (manager.location!.timestamp.isInside(date: now, granularity: .day) && manager.location!.timestamp.isBeforeDate(now, granularity: .hour))
//            {
//                KeepersWebFilteringLoggerWriter.log("location timestamp \(manager.location!.timestamp)")
//                KeepersWebFilteringLoggerWriter.log("location sent to the server is not today or has an earlier time than now: \(now)")
//                return
//            }
        }
        
//        scheduleLocalNotification(alert: "Updating Location")
        
        
        
//        let defaultRegion = locationManagerInstance.monitoredRegions.filter{$0.identifier == region_allRegion}
//
//
//        if  defaultRegion.count == 0 {
//            createNewRegionsForLocationTracking(location: manager.location!)
//
//        }else if defaultRegion.count != 0 {
//
//            let circularRegion = defaultRegion.first! as! CLCircularRegion
//            let center = CLLocation(latitude: circularRegion.center.latitude, longitude: circularRegion.center.longitude)
//
//            let distance = manager.location!.distance(from: center)
////            print("distance = \(distance) :: radius = \(circularRegion.radius)")
//
//            if distance > largeRegionRadius + 65 { //margin of error is 65
//                KeepersWebFilteringLoggerWriter.log("distance > 2 * circularRegion.radius")
//                createNewRegionsForLocationTracking(location: manager.location!)
//            }
            
//        }
        
    }
    
//    func createNewRegionsForLocationTracking(location:CLLocation){
//
//        removeRegionsForLocationTracking()
//
//        KeepersWebFilteringLoggerWriter.log("in createNewRegionsForLocationTracking, creating 5 new regions")
//        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 0),regionRadius: smallRegionRadius, identifier: region1ID)
//        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 90),regionRadius: smallRegionRadius, identifier: region2ID)
//        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 180),regionRadius: smallRegionRadius, identifier: region3ID)
//        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 270),regionRadius: smallRegionRadius, identifier: region4ID)
//        self.createRegion(location: location ,regionRadius: largeRegionRadius, identifier: region_allRegion)
//
//    }
    func saveLocation(_ location: CLLocation){
        var locationsArray : [CLLocation] = []
        if UserDefaults.standard.data(forKey: "LocationsArray") != nil  {
             locationsArray = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.data(forKey: "LocationsArray")!)as? [CLLocation] ?? []
        }
       
        locationsArray.append(location)
        let data = NSKeyedArchiver.archivedData(withRootObject: locationsArray)
        UserDefaults.standard.set(data, forKey: "LocationsArray")
    }
    
    func removeRegionsForLocationTracking(){
        KeepersWebFilteringLoggerWriter.log("in removeRegionsForLocationTracking, removing the 5 regions")
        for region in locationManagerInstance.monitoredRegions {
            if region.identifier.contains("region_"){
                locationManagerInstance.stopMonitoring(for: region)
            }
        }
    }
    
    func getCordInDirection(location:CLLocation ,directionInDegrees:Int, distance: Int)->CLLocation{

        let distanceInMeter : Int = distance

        let lat = location.coordinate.latitude
        let long = location.coordinate.longitude

        let radDirection : CGFloat = Double(directionInDegrees).degreesToRadians

        let dx = Double(distanceInMeter) * cos(Double(radDirection))
        let dy = Double(distanceInMeter) * sin(Double(radDirection))

        let radLat : CGFloat = Double(lat).degreesToRadians

        let deltaLongitude = dx/(111320 * Double(cos(radLat)))
        let deltaLatitude = dy/110540

        let endLat = lat + deltaLatitude
        let endLong = long + deltaLongitude
        
        return CLLocation(latitude: endLat as CLLocationDegrees, longitude: endLong as CLLocationDegrees)

    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        KeepersWebFilteringLoggerWriter.log("6: didEnterRegion, region: \(region)")
        
        guard let location = manager.location else {return}
        
        let coordinate = location.coordinate

        KeepersWebFilteringLoggerWriter.log("6.5: latitude, longitude =   (\(coordinate.latitude), \(coordinate.longitude))")
        //child is logged in
        //sabe location
        
        //        scheduleLocalNotification(alert: "Did Enter Region")
        
        if region.identifier.contains("region_"){
            KeepersWebFilteringLoggerWriter.log("saving location entered")
            saveLocation(location)
        }
        KeepersWebFilteringLoggerWriter.log("keepers' entered region (not parent region), calling createNewRegionsForLocationTracking")
        createNewRegionsForLocationTracking(location: location)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        KeepersWebFilteringLoggerWriter.log("8: didExitRegion, region: \(region)")
        
        if let location = manager.location, region.identifier.contains("region_") {
            KeepersWebFilteringLoggerWriter.log("9.5: exited latitude, longitude =   (\(location.coordinate.latitude), \(location.coordinate.longitude))")
            
            
            KeepersWebFilteringLoggerWriter.log("saving location exited")
            saveLocation(location)
            
            createNewRegionsForLocationTracking(location: location)
            
        }

    }
    
//    func clearAllRegions(){
//        //Clear only the Restricted or NotRestricted Areas
//        for region in locationManagerInstance.monitoredRegions {
//            if region.identifier.contains("NotRestricted") || region.identifier.contains("Restricted"){
//                locationManagerInstance.stopMonitoring(for: region)
//            }
//        }
//    }
    
//    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
//        KeepersWebFilteringLoggerWriter.log("6: didEnterRegion, region: \(region)")
//
//        guard let location = manager.location else {return}
//
//            let coordinate = location.coordinate
//
//            if region.identifier.contains("region_"){
//                KeepersWebFilteringLoggerWriter.log("keepers' region (not parent region), calling sendLocationToServer")
//                var regionsEntered = UserDefaults.standard.object(forKey: "didEnterRegionLocation") as? [CLCircularRegion] ?? []
//                if region as? CLCircularRegion != nil {
//                    regionsEntered.append(region as! CLCircularRegion)
//                    UserDefaults.standard.setValue(regionsEntered, forKey: "didEnterRegionLocation")
//                }
//
//                var locationsArray = UserDefaults.standard.object(forKey: "LocationsArray") as? [CLLocation] ?? []
//                locationsArray.append(location as! CLLocation)
//                UserDefaults.standard.setValue(locationsArray, forKey: "LocationsArray")
//
//
//                KeepersWebFilteringLoggerWriter.log("keepers' region (not parent region), calling createNewRegionsForLocationTracking")
//                createNewRegionsForLocationTracking(location: location)
//            }
//
//    }
    
//    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
//        KeepersWebFilteringLoggerWriter.log("8: didExitRegion, region: \(region)")
////        scheduleLocalNotification(alert: "Did Exit Region")
//        guard let location = manager.location else {return}
//        var regionsExited = UserDefaults.standard.object(forKey: "didExitRegionLocation") as? [CLCircularRegion] ?? []
//        if region as? CLCircularRegion != nil {
//            regionsExited.append(region as! CLCircularRegion)
//            UserDefaults.standard.setValue(regionsExited, forKey: "didExitRegionLocation")
//        }
//
//        var locationsArray = UserDefaults.standard.object(forKey: "LocationsArray") as? [CLLocation] ?? []
//        locationsArray.append(location as! CLLocation)
//        UserDefaults.standard.setValue(locationsArray, forKey: "LocationsArray")
////            scheduleLocalNotification(alert: "Exit : I will send the request")
//
//
//            //if this region only for tracking the offline mode then delete it
//            //else it is a region created by parent for this child
//            manager.startUpdatingLocation()
//
//    }
    func createNewRegionsForLocationTracking(location:CLLocation){
        
        
        KeepersWebFilteringLoggerWriter.log("in createNewRegionsForLocationTracking, creating 5 new regions, (lat, long) = (\(location.coordinate.latitude) , \(location.coordinate.longitude))")
        removeRegionsForLocationTracking()
        
        self.createRegion(location: location ,regionRadius: smallRegionRadius, identifier: current_region)
        let smallDistance = 100 + Int(smallRegionRadius)
        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 0, distance: smallDistance),regionRadius: smallRegionRadius, identifier: region1ID)
        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 90, distance: smallDistance),regionRadius: smallRegionRadius, identifier: region2ID)
        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 180, distance: smallDistance),regionRadius: smallRegionRadius, identifier: region3ID)
        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 270, distance: smallDistance),regionRadius: smallRegionRadius, identifier: region4ID)
        
        //large regions
        let largeDistance = 200 + Int(largeRegionRadius)
//        self.createRegion(location: location ,regionRadius: largeRegionRadius, identifier: region_allRegion)
        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 0, distance: largeDistance),regionRadius: largeRegionRadius, identifier: region1ID_BIG)
        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 90, distance: largeDistance),regionRadius: largeRegionRadius, identifier: region2ID_BIG)
        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 180, distance: largeDistance),regionRadius: largeRegionRadius, identifier: region3ID_BIG)
        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 270, distance: largeDistance),regionRadius: largeRegionRadius, identifier: region4ID_BIG)
        
        //outer regions
        let outerDistance = 300 + Int(largeRegionRadius)
        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 0, distance: outerDistance),regionRadius: largeRegionRadius, identifier: region1ID_BIG_OUTER)
        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 45, distance: outerDistance),regionRadius: largeRegionRadius, identifier: region2ID_BIG_OUTER)
        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 90, distance: outerDistance),regionRadius: largeRegionRadius, identifier: region3ID_BIG_OUTER)
        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 135, distance: outerDistance),regionRadius: largeRegionRadius, identifier: region4ID_BIG_OUTER)
        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 180, distance: outerDistance),regionRadius: largeRegionRadius, identifier: region5ID_BIG_OUTER)
        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 225, distance: outerDistance),regionRadius: largeRegionRadius, identifier: region6ID_BIG_OUTER)
        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 270, distance: outerDistance),regionRadius: largeRegionRadius, identifier: region7ID_BIG_OUTER)
        self.createRegion(location: getCordInDirection(location: location, directionInDegrees: 315, distance: outerDistance),regionRadius: largeRegionRadius, identifier: region8ID_BIG_OUTER)
    }
    
    func createRegion(longitude:Double, latitude:Double, id:Int64, regionRadius:Double, isRestricted:Bool){
        var identifier = ""
//        if isRestricted {
//            identifier = "Restricted"
//        }else{
//            identifier = "NotRestricted"
//        }
        identifier += String(id)
        createRegion(location: CLLocation(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude)), regionRadius:regionRadius, identifier: identifier)
    }
    
    func createRegion(location:CLLocation?, regionRadius:Double, identifier:String ) {
        KeepersWebFilteringLoggerWriter.log("20: createRegion, location: \(location), regionRadius: \(regionRadius),  identifier: \(identifier)")
        guard let location = location else {
            // Logger.write(text: "Problem with location in creating region", to: kLogsFile)
//            scheduleLocalNotification(alert: "Bad Location")
            return
        }
        if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            
            let coordinate = CLLocationCoordinate2DMake((location.coordinate.latitude), (location.coordinate.longitude))
            
            let region = CLCircularRegion(center: CLLocationCoordinate2D(
                latitude: coordinate.latitude,
                longitude: coordinate.longitude),
                                          radius: regionRadius,
                                          identifier: identifier)
            
            // By default, both are true but we set both in case
            region.notifyOnEntry = true
            region.notifyOnExit = true
                        
            self.locationManagerInstance.startMonitoring(for: region)
        }
        else {
//            scheduleLocalNotification(alert: "Bad Location")
        }
}

}

