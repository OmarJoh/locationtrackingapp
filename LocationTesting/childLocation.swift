//
//  childLocation.swift
//  keepers
//
//  Created by Noy's Mac on 7.1.2017.
//  Copyright © 2017 Keepers. All rights reserved.
//

import UIKit
import MapKit

protocol childLocationProtocol {
    func drawPointsAndLines(locationList: [CLLocation])
    func drawTimeLineOnMap (locationList: [CLLocation])
}

class childLocation: UIViewController, UITableViewDataSource, UITextFieldDelegate {
    static var storyboardName: String = "ParentSide"
    
    @IBOutlet weak var showLocationsBtn: UIButton!
    @IBOutlet weak var locationsTF: UITextField!
    @IBOutlet weak var hideShowContainerView: UIView!
    @IBOutlet weak var hideShowBtn: UIButton!
    @IBOutlet weak var deleteLocations: UIButton!
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
            if motion == .motionShake{
                print("in motionEnded")
//    print("WF-SDK appGroupStr = \(KeepersLogger.getAppGroup())")
                print( KeepersWebFilteringLoggerWriter.readLog())
            }
        
        }
    
    
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var tableviewContainerHeight: NSLayoutConstraint!
    @IBOutlet fileprivate weak var centerBtn: UIButton!
//    @IBOutlet fileprivate weak var backBtn: UIButton!
//    @IBOutlet fileprivate weak var childName: UILabel!
//    @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!
//    @IBOutlet weak var chooseDateContainerView: UIView!
//    @IBOutlet fileprivate weak var chooseDateBtn: UIButton!
    @IBOutlet fileprivate weak var timeLineTable: UITableView!
    @IBOutlet fileprivate weak var map: MKMapView!
//    @IBOutlet fileprivate weak var topColorView: UIView!
//    @IBOutlet fileprivate weak var selectedDataLabel: UILabel!
    @IBOutlet weak var tableViewContainer: UIView!
    
//    @IBOutlet weak var lowAlphaView: UIView!
    
    @IBOutlet weak var noDataLabel: UILabel!
    
//    @IBOutlet weak var titleAndDateContainerView: UIView!
    @IBOutlet weak var dropDownContainerView: UIView!
    fileprivate var locationViewModel : childLocationViewModel!
//
//    lazy var loadingIndicatorView : LoadingIndicator = {
//        return LoadingIndicator(view: self.view)
//
//    }()
    

    // MARK: - Properties
    var hasSetPointOrigin = false
    var pointOrigin: CGPoint?
    var minContainerHeight = CGFloat(0)
    fileprivate var timer = Timer()
    fileprivate var counter = 1
    fileprivate var accuracyView : UIView?
    fileprivate var shouldOpenLocationList = true
    var locationDataSource = [CLLocation]()
    var locationsByNumber: Int =  UserDefaults.standard.integer(forKey: UserDefKeys.LogfileNumber.rawValue)
    var allLocations = [[CLLocation]]()
//    let locationDataSource = UserDefaults.standard.object(forKey: "LocationsArray") as? [CLLocation] ?? []
    var addresses = [String]()
    var locationsDic = [String : [CLLocation]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let dateString = formatter.string(from: Date())
        let number: String = String(UserDefaults.standard.integer(forKey: UserDefKeys.LogfileNumber.rawValue))
        locationsTF.placeholder = "\(dateString)-\(number)"
        locationsTF.text = "\(dateString)-\(number)"
        locationViewModel = childLocationViewModel(delegate: self)
        if UserDefaults.standard.data(forKey: UserDefKeys.LocationsArray.rawValue) != nil  {
            locationDataSource = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.data(forKey: UserDefKeys.LocationsArray.rawValue)!)as? [CLLocation] ?? [CLLocation]()
        }
        getAdresses()
        NotificationCenter.default.addObserver(self, selector: #selector(newLocationReceivedHandler), name: NSNotification.Name( "Got A new Location to update map"), object: nil)
//        locationViewModel = childLocationViewModel(delegate: self)
        //Setup timeLineTable tableView :
        timeLineTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
//        timeLineTable.contentInset = UIEdgeInsets(top: 60, left: 0, bottom: 0, right: 0)
        //Setup date in choosen date
        addButtonEnlargmentView()
//        setupChoosenDateButton()
        //get info about child
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(didLongPress))
        map.addGestureRecognizer(longPress)
        map.delegate = self
        

        askRestrictedAreasFromServer()
        let locationListPanGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerAction))
        tableViewContainer.addGestureRecognizer(locationListPanGesture)
        let locationListArrowTapGesture = UITapGestureRecognizer(target: self, action: #selector(locationListArrowClicked))
        dropDownContainerView.addGestureRecognizer(locationListPanGesture)
        dropDownContainerView.addGestureRecognizer(locationListArrowTapGesture)

        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    @IBAction func newLocationsRestartClicked(_ sender: Any) {
        allLocations.append(locationDataSource)
        KeepersWebFilteringLoggerWriter.newLogfile()
        //
        let data = NSKeyedArchiver.archivedData(withRootObject: [])
        UserDefaults.standard.set(data, forKey: UserDefKeys.LocationsArray.rawValue)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let dateString = formatter.string(from: Date())
        let number: String = String(UserDefaults.standard.integer(forKey: UserDefKeys.LogfileNumber.rawValue))
        //
        locationsTF.placeholder = "\(dateString)-\(number)"
        locationsTF.text = "\(dateString)-\(number)"
        //
        locationsDic["\(dateString)-\(number)"] = locationDataSource
        let dataDic = NSKeyedArchiver.archivedData(withRootObject: locationsDic)
        
        UserDefaults.standard.set(dataDic, forKey: UserDefKeys.allLocationsDictionary.rawValue)
        locationDataSource = []
        viewDidAppear(true)
    }
    
    @IBAction func showLocationsClicked(_ sender: Any) {
        locationsTF.resignFirstResponder()
        if let allLocationsDicData = UserDefaults.standard.data(forKey: UserDefKeys.allLocationsDictionary.rawValue) {
        locationsDic = NSKeyedUnarchiver.unarchiveObject(with: allLocationsDicData)as? [String : [CLLocation]] ?? [String : [CLLocation]]()
        locationDataSource = locationsDic[locationsTF.text ?? ""] ?? []
        }
        viewDidAppear(true)
    }
    
    @IBAction func hideShowButtonsClicked(_ sender: Any) {
        let text = hideShowContainerView.isHidden == false ? "S" : "H"
        hideShowBtn.setTitle(text, for: .normal)
        hideShowContainerView.isHidden.toggle()
        deleteLocations.isHidden.toggle()
    }
    
    @IBAction func deleteLocationsClicked(_ sender: Any) {
        if let allData = UserDefaults.standard.data(forKey: UserDefKeys.allLocationsDataSource.rawValue){
            var allLocationDataSource = NSKeyedUnarchiver.unarchiveObject(with: allData)as? [CLLocation] ?? []
            if let locationDataSourceData = UserDefaults.standard.data(forKey: UserDefKeys.LocationsArray.rawValue) {
            let locationDataSource = NSKeyedUnarchiver.unarchiveObject(with: locationDataSourceData)as? [CLLocation] ?? []
            allLocationDataSource.append(contentsOf: locationDataSource)
            var data = NSKeyedArchiver.archivedData(withRootObject: allLocationDataSource)
            
            UserDefaults.standard.set(data, forKey: UserDefKeys.allLocationsDataSource.rawValue)
            
            data = NSKeyedArchiver.archivedData(withRootObject: [])
            UserDefaults.standard.set(data, forKey: UserDefKeys.LocationsArray.rawValue)
            }
        } else {
            var allLocationDataSource = [CLLocation]()
            if let locationDataSourceData = UserDefaults.standard.data(forKey: UserDefKeys.LocationsArray.rawValue) {

            let locationDataSource = NSKeyedUnarchiver.unarchiveObject(with: locationDataSourceData)as? [CLLocation] ?? []
        allLocationDataSource.append(contentsOf: locationDataSource)
        var data = NSKeyedArchiver.archivedData(withRootObject: allLocationDataSource)
        
        UserDefaults.standard.set(data, forKey: UserDefKeys.allLocationsDataSource.rawValue)
        
        data = NSKeyedArchiver.archivedData(withRootObject: [])
        
        UserDefaults.standard.set(data, forKey: UserDefKeys.LocationsArray.rawValue)
            }
        }
        self.locationDataSource = []
        viewDidAppear(true)
    }
    
    @IBAction func allLocationsClicked(_ sender: Any) {
        if let allData = UserDefaults.standard.data(forKey: UserDefKeys.allLocationsDataSource.rawValue){
        let allLocationDataSource = NSKeyedUnarchiver.unarchiveObject(with: allData)as? [CLLocation] ?? []
            self.locationDataSource = allLocationDataSource
        }
        viewDidAppear(true)
    }
    
    @IBAction func latestLocationsClicked(_ sender: Any) {
        if let allData = UserDefaults.standard.data(forKey: UserDefKeys.LocationsArray.rawValue){
        let locationDataSource = NSKeyedUnarchiver.unarchiveObject(with: allData)as? [CLLocation] ?? []
            self.locationDataSource = locationDataSource
        }
        viewDidAppear(true)
    }
    
    func getAdresses() {
        for location in locationDataSource {
            LocationHelperUtil.getAdressName(coords: location, onComplete: {address in
                self.addresses.append(address)
                self.timeLineTable.reloadData()
            })
        }
    }
    
    @objc func panGestureRecognizerAction(sender: UIPanGestureRecognizer) {

        
        let location = sender.location(in: self.view).y
                
        let newHeight = pointOrigin!.y - (location - minContainerHeight)
        
        let rows = locationDataSource.count
        var maxHeight = CGFloat(timeLineTable.rowHeight) * CGFloat(rows)
        maxHeight = min(maxHeight, self.view.frame.height - 80 - 20)
        let maxHeightAllowed = self.view.frame.height - 80 - 20

        if newHeight > maxHeight &&  maxHeight > minContainerHeight{
            tableviewContainerHeight.constant = maxHeight
        }else if newHeight < minContainerHeight {
            tableviewContainerHeight.constant = minContainerHeight
            shouldOpenLocationList = true
        }else if newHeight < maxHeightAllowed {
            tableviewContainerHeight.constant = newHeight
        }
        
    }
    
    @objc func locationListArrowClicked(sender: UITapGestureRecognizer) {

        if locationDataSource.count <= 1 {
            return
        }
        
        let location = sender.location(in: self.view).y
                
        let newHeight = pointOrigin!.y - (location - minContainerHeight)
        
        let rows = locationDataSource.count
        var maxHeight = CGFloat(timeLineTable.rowHeight) * CGFloat(rows)
        maxHeight = min(maxHeight, self.view.frame.height - 80 - 20)
        

        if shouldOpenLocationList{
            tableviewContainerHeight.constant = maxHeight
        }else {
            tableviewContainerHeight.constant = minContainerHeight
        }
        shouldOpenLocationList.toggle()
    }
    
    @objc func closeCalendarView(_ sender:UITapGestureRecognizer?){
//        self.lowAlphaView.isHidden = true
    }

    func drawCircleInsideTableViewContainer(){
      let halfSize:CGFloat = CGFloat(3)
      let desiredLineWidth:CGFloat = 1
      var i = 0
      var lastPosition = 0
      while i <= 70 {
        i = i + 1
        let circlePath = UIBezierPath(
            arcCenter: CGPoint(x:halfSize + 30 ,y:halfSize + (CGFloat(i) * (halfSize*2 + 5) ) + 5),
            radius: CGFloat( halfSize - (desiredLineWidth/2) ),
            startAngle: CGFloat(0),
            endAngle:CGFloat(Double.pi * 2),
            clockwise: true)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = UIColor(displayP3Red: 172/255.0, green: 172/255.0, blue: 172/255.0, alpha: 1.0).cgColor
        shapeLayer.strokeColor = UIColor(displayP3Red: 172/255.0, green: 172/255.0, blue: 172/255.0, alpha: 1.0).cgColor
        shapeLayer.lineWidth = desiredLineWidth
        tableViewContainer.layer.addSublayer(shapeLayer)
        lastPosition = lastPosition + 2
      }
        timeLineTable.layer.zPosition = 2
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !hasSetPointOrigin {
            hasSetPointOrigin = true
            pointOrigin = self.tableViewContainer.frame.origin
            minContainerHeight = self.view.frame.height - self.tableViewContainer.frame.origin.y
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        initLocationForChild()
    }
    
    @IBAction func showGeofenceHelperView(_ sender: Any) {
//        showTurotialIfNeeded(forceShow:true)
    }
    
    
    func initLocationForChild(){
        //        activityIndicator.startAnimating()
        drawTimeLineOnMap(locationList: locationDataSource)
        askRestrictedAreasFromServer()
    }
    
    func addBlurView(){
        
        tableViewContainer.backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: .light)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        tableViewContainer.insertSubview(blurView, at: 0)
        
        blurView.alpha = 0.93
        
        NSLayoutConstraint.activate([
        blurView.heightAnchor.constraint(equalTo: tableViewContainer.heightAnchor),
        blurView.widthAnchor.constraint(equalTo: tableViewContainer.widthAnchor),
        ])

    }
    
    func createShadow(){
        
        
    }
    
    @objc fileprivate func newLocationReceivedHandler() {
        if let locationDataSourceData = UserDefaults.standard.data(forKey: UserDefKeys.LocationsArray.rawValue) {
            locationDataSource = NSKeyedUnarchiver.unarchiveObject(with: locationDataSourceData)as? [CLLocation] ?? []
            drawTimeLineOnMap(locationList: locationDataSource)
            self.timeLineTable.reloadData()
        }
    }
    
    func askRestrictedAreasFromServer () {
//        var userDefaultsService: UserDefaultsServiceChild = UserDefaultsServiceChild()
        let timeBetweenUpdates : Int64 = 3*60*1000
//        userDefaultsService = UserDefaultsServiceChild()
//        let last_time_Updated_Server = userDefaultsService.last_time_Updated_Server
//        if timeBetweenUpdates != 0 {
//            if Date().millisecondsSince1970 - last_time_Updated_Server < timeBetweenUpdates {
//                return
//            }
//        }
//        userDefaultsService.set(last_time_Updated_Server: Date().millisecondsSince1970)
        
//        parentAPI.getAllGeofenceAreasForChild(childId){ list in
//            if let geofenceList = list {
//                self.locationViewModel.restrictedAreasDataSource = geofenceList.map { $0.convertToRestrictedAreaOverlay() }
//                self.locationViewModel.restrictedAreasDataSource.forEach({ let _ = self.map.addAreaAndAnnotation(from: $0) })
//            }
//        }
    }
    
    func addButtonEnlargmentView () {  // create a bigger wrapper for opening the date-picker
    }
    
    @objc func didLongPress(gestureRecognizer: UIGestureRecognizer) {
        
    }
    
    //this func is never called but can be useful in the future.
    func radiusTooSmallAlert() {
        
    }
    
    @IBAction func onClickCurrentDate(_ sender: Any) {
        chooseDateClicked()
    }
    
    @objc func chooseDateClicked() {
        
        
    }
    
    fileprivate func createAccuracyLevelView (with accuracy : AccuracyLevel) -> UIView? {
        if willUpdateAccuracyLevelView(with: accuracy) { return nil }
        let accuracyViewHeight: CGFloat = 50
        let accuracyImageWidth: CGFloat = 50
        let padding:            CGFloat = 4
        var accuracyLabelX:     CGFloat = 0
        let screenSize          = UIScreen.main.bounds
       
        let accuracyImage       = UIImageView(image: UIImage(named: "Accuracy-" + accuracy.rawValue))
        let accuracyLabel       = UILabel()
        accuracyLabel.text = accuracy.localizedFullSentence()
        accuracyLabel.numberOfLines = 0
        accuracyLabel.sizeToFit()
        
        // find out if the text is too long - if so enlarge accuracyView
        let labelHeight: CGFloat = 30
        accuracyLabel.frame = CGRect(x: accuracyLabelX, y: (accuracyViewHeight - labelHeight)/2 , width: accuracyLabel.frame.width, height: labelHeight)
        if (labelHeight + ( 2 * padding ) ) > accuracyViewHeight {
        }
        accuracyView?.backgroundColor = UIColor(white: 1, alpha: 0.8)
        accuracyView?.addSubview(accuracyImage)
        accuracyView?.addSubview(accuracyLabel)
        return accuracyView
    }
    func willUpdateAccuracyLevelView (with accuracy : AccuracyLevel) -> Bool {
        guard let accuracyView = self.accuracyView else { return false }
        guard let accuracyImage = accuracyView.subviews.first(where: {$0 is UIImageView}) as? UIImageView,
              let accuracyLabel = accuracyView.subviews.first(where: {$0 is UILabel}) as? UILabel else { return false }
        guard accuracyLabel.text != accuracy.localizedFullSentence() else { return true }
        accuracyImage.image = UIImage(named: "Accuracy-" + accuracy.rawValue)
        accuracyLabel.text = accuracy.localizedFullSentence()
        return true
    }
    @IBAction func onClickCenter(_ sender: Any) {
        guard let firstLoc = locationDataSource.first else {return}
        map.setRegion(at: firstLoc)
    }

    @objc func animate() {
        guard timer.isValid, let pImgView = timer.userInfo as? UIImageView else { return }
        pImgView.image = UIImage(named: "frame_pulse_" + counter.description + "_delay-0.2s")
        counter = counter == 16 ? 0 : counter + 1
    }
    @IBAction func didTapBackButton(_ sender: UIButton) {
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: table view delegate
extension childLocation : UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.timeLineTable.dequeueReusableCell(withIdentifier: "timeLineCell") as? TimeLineTableViewCell ??
                        TimeLineTableViewCell(style: .default, reuseIdentifier: "timeLineCell")
        let _ = getCellForRow(at: indexPath.row, on: cell)
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationDataSource.count
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row < locationDataSource.count else { return }
        map.setRegion(at: locationDataSource[indexPath.row])
        tableviewContainerHeight.constant = timeLineTable.rowHeight
        shouldOpenLocationList = true
        timeLineTable.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
}
// MARK: map view delegate
extension childLocation : MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
//        if let restrictedAnnotation = view.annotation as? RestrictedAreaAnnotation {
//            let area = self.locationViewModel.restrictedAreasDataSource.first { $0.coordinate == restrictedAnnotation.coordinate }
//            if area == nil { return }
//            let alert = RemoveGeofencingPopup(superview: self.view) { shouldRemove in
//                guard shouldRemove else { return }
//                self.locationViewModel.removeGeofenceAreaFromServer(geofenceId: area!.id) { isSuccess in
//                    guard isSuccess else { self.locationViewModel.createServerErrorMessage(controller: self) ; return }
//                    self.locationViewModel.restrictedAreasDataSource.removeAll(where: {$0 == area!})
//                    //start re-draw
//                    self.map.removeAll()
//                    self.drawPointsAndLines(locationList: self.locationDataSource)
//                    self.locationViewModel.restrictedAreasDataSource.forEach({ let _ = self.map.addAreaAndAnnotation(from: $0) })
//                }
//            }
//            self.view.addSubview(alert)
//            self.view.sendSubviewToBack(self.map)
//        } else {
//            guard let firstLocationCoordinates = locationDataSource.first?.coordinate else {return}
//            let firstlocation = CLLocationCoordinate2D(latitude: firstLocationCoordinates.latitude , longitude: firstLocationCoordinates.longitude)
//            if let latestAnnotation = mapView.annotations.first(where: {$0.coordinate == firstlocation}) {
//                self.map.selectAnnotation(latestAnnotation, animated: true)
//            }
//        }
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let renderer = mapView.createRestrictedAreaRenderer(on:overlay){ return renderer }
        return mapView.createPolylineRenderer(on:overlay)
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let restrictedAnnotation = annotation as? RestrictedAreaAnnotation { return mapView.viewFor(annotation: restrictedAnnotation) }
        if let customAnnotation     = annotation as? CustomPointAnnotation    { return mapView.viewFor(annotation: customAnnotation)     }
        return nil
    }
}
extension childLocation : childLocationProtocol {
    func drawPointsAndLines(locationList: [CLLocation]) {
        let pinImageName = locationViewModel.getPinImageName(isTrail: false)
        let title =  "Child's location"
        var i = 0
        for location in locationList {
            i += 1
             let coordinate = location.coordinate
                if location == locationList.first {
                    let pointEndAnnotation = CustomPointAnnotation(imageName: pinImageName, at: coordinate, title: title)
                    self.map.addAnnotation(with: pointEndAnnotation)
                    self.map.selectAnnotation(pointEndAnnotation, animated: false)
                    addAccuracyView(for : location)
                }else{
                    let pinImageName = locationViewModel.getPinImageName(isTrail: true)
                    let pointTrailAnnotation = CustomPointAnnotation(imageName: "MapDotIcon", at: coordinate)
                    self.map.addAnnotation(with: pointTrailAnnotation)
                }
            
        }
        map.addOverlay( MKPolyline(from: locationList) )// draw lines between trail location
        if locationList.first != nil {map.setRegion(at: locationList.first!)} // center map on latest location
        timeLineTable.reloadData()
//        activityIndicator.stopAnimating()
//        loadingIndicatorView.stopAnimating()
    }
    fileprivate func addAccuracyView(for location: CLLocation) {
        return
//        guard let accuracyView = createAccuracyLevelView(with: location.getAccuracy()) else { return }
//        self.view.addSubview(accuracyView)
//        if let overlay = view.viewWithTag(448) { view.bringSubviewToFront(overlay) }
    }
    func drawTimeLineOnMap(locationList: [CLLocation]) {
        closeCalendarView(nil)
        removeNonResctrictedAnnotationsAndOverlays()
//        centerBtn.isHidden = locationList.isEmpty
        if locationList.isEmpty { //show "no history avilable" in tableView first cell
            timeLineTable.reloadData()
//            activityIndicator.stopAnimating()
//            loadingIndicatorView.stopAnimating()
            noDataLabel.isHidden = false
            noDataLabel.text = "No location available for this period."
            
            ///Clearing small Circles view
            for layer in tableViewContainer.layer.sublayers ?? [] {
                if layer is CAShapeLayer {
                    layer.removeFromSuperlayer()
                }
            }
            ///
            
            return
        }
        noDataLabel.isHidden = true
        drawCircleInsideTableViewContainer()
        drawPointsAndLines(locationList: locationList)
    }
    fileprivate func removeNonResctrictedAnnotationsAndOverlays () {
        let filtredAnnotations = self.map.annotations.filter{!$0.isKind(of: RestrictedAreaAnnotation.self)}
        self.map.removeAnnotations(filtredAnnotations)
        let filtredOverLays = self.map.overlays.filter { !$0.isKind(of: RestrictedAreaOverlay.self) }
        self.map.removeOverlays(filtredOverLays)
    }
    
    
    func getCellForRow(at index:Int, on cell : TimeLineTableViewCell) -> (cell : TimeLineTableViewCell ,timerOption : Int) {
        let maxIndex = locationDataSource.count
        guard  index < maxIndex else {  return (cell,0) }
        let locationForCell = locationDataSource[index]
        var timerOption = 0
        cell.childAddress.text = addresses.count > index ? addresses[index] : ""
        if (index == 0){ //first cell
            cell.chainImg.image = UIImage(named: "LastSeenIcon")
            cell.locationNumberLabel.isHidden = true
                
                cell.chainImg.isHidden = false
                cell.lastSeen.isHidden = false
                cell.timeLbl.isHidden = false

                let timeStamp = locationForCell.timestamp
                if timeStamp == Date(timeIntervalSince1970: 0) {
                    cell.timeLbl.text = "Now"
                } else {
                    cell.timeLbl.text = readableTimestamp(from: timeStamp)
                }
            
        } else {
            cell.locationNumberLabel.text = ""
            cell.chainImg.image = UIImage(named: "MapDotIcon")
            cell.locationNumberLabel.isHidden = false
            cell.chainImg.isHidden = false
            cell.timeLbl.isHidden = false
            cell.lastSeen.isHidden = false
            let imageName = index == (maxIndex - 1) ? "endOFRoad" : "new_points_2"
            var timeStr: String = ""
            let date = locationForCell.timestamp
                if date == Date(timeIntervalSince1970: 0) {
                    timeStr = "Now"
                }else{
                    timeStr = readableTimestamp(from: date)
                }
            
            cell.timeLbl.text = timeStr.isEmpty ? "Now" : timeStr
        }
        return (cell,timerOption)
    }
    
    func readableTimestamp (from date : Date) -> String {
        var timeStr: String = ""
        let comp = Calendar.current.dateComponents([.hour, .minute], from: date)
        if let hour = comp.hour, let minute = comp.minute {
            timeStr += hour < 10    ? "0" : ""
            timeStr += hour.description + ":"
            timeStr += minute < 10  ? "0" : ""
            timeStr += minute.description
        }
        return timeStr
    }
}



class childLocationViewModel {
    
    var delegate : childLocationProtocol
    
    init(delegate : childLocationProtocol) {
        self.delegate = delegate
    }
    
    func getButtonEnlargment(between backMaxX : CGFloat , and indicatorMinX : CGFloat,_  complitionHadnler : @escaping () -> () ) -> UIButton {
        let width =  (indicatorMinX - backMaxX) / 2
        let height = CGFloat(69) // entire TopColorView is 70
        let btnEnlargmentView = UIButton(frame: CGRect(x: backMaxX, y: CGFloat(0) , width:width , height: height))
        btnEnlargmentView.backgroundColor = UIColor.clear
        btnEnlargmentView.setTitle("", for: .normal)
        btnEnlargmentView.addAction(for: .touchUpInside, complitionHadnler)
        return btnEnlargmentView
    }
    
    func getPinImageName(isTrail: Bool) -> String {
        return isTrail ? "Point_to_map" :
             "New_GPS_ios"
    }
    
    
}
